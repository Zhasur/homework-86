const express = require('express');
const Track = require('../models/Track');

const router = express.Router();

router.get('/:id', (req, res) => {
    Track.find({albums: req.params.id}).sort({trackNumber: 1}).populate('albums')
        .then(track => res.send(track))
        .catch(() => res.sendStatus(500))
});

module.exports = router;