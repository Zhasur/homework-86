const express = require('express');

const auth = require('../middleware/auth');
const User = require('../models/User');


const router = express.Router();

router.post('/', (req, res) => {
    const user = new User(req.body);
    user.generateToken();

    user.save()
        .then(user => res.send({message: "User registered" , user}))
        .catch(error => res.status(400).send(error))
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(400).send({error: 'Username not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Password is wrong'});
    }

    user.generateToken();

    return res.send({message: "Login success", user});
});

router.get('/secret', auth, (req, res) => {
    console.log(req.user._id);

    res.send({message: 'OK'});
});

module.exports = router;
