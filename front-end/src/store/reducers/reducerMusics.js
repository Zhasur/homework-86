import {FETCH_ARTIST_SUCCESS} from "../actions/ArtistsActions";
import {FETCH_ALBUM_SUCCESS} from "../actions/AlbumActions";
import {FETCH_TRACKS_SUCCESS} from "../actions/TracksAction";

const initialState = {
    artists: [],
    albums: [],
    tracks: []
};

const reducerMusics = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTIST_SUCCESS:
            return {...state, artists: action.artists};
        case FETCH_ALBUM_SUCCESS:
            return {...state, albums: action.albums};
        case  FETCH_TRACKS_SUCCESS:
            return {...state, tracks: action.tracks};
        default:
            return state;
    }
};

export default reducerMusics;