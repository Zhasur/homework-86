import axios from '../../axios-musics'

export const FETCH_ARTIST_SUCCESS = "FETCH_ARTIST_SUCCESS";
export const FETCH_ARTIST_ERROR = "FETCH_ARTIST_ERROR";

const fetchArtistSuccess = (artists) => ({type: FETCH_ARTIST_SUCCESS, artists});
const fetchArtistError = () => ({type: FETCH_ARTIST_ERROR});

export const fetchSuccess = () => {
    return dispatch => {
        return axios.get('/artists')
            .then(response => {
                dispatch(fetchArtistSuccess(response.data));
            })
            .catch(error => {
                dispatch(fetchArtistError(error))
            })
    }
};
