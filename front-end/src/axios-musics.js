import axios from 'axios'
import {urlMusics} from "./constants";

const istance = axios.create({
    baseURL: urlMusics
});

export default istance;