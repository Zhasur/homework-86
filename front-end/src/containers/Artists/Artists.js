import React, {Component} from 'react';
import {fetchSuccess} from "../../store/actions/ArtistsActions";
import {connect} from "react-redux";
import {Card, CardBody, CardImg} from "reactstrap";

import './Artists.css'
import {Link} from "react-router-dom";

class Artists extends Component {
    componentDidMount() {
        this.props.fetchArtists();
    }

    render() {
        console.log(this.props.artists);
        const artists = this.props.artists.map(artist => {
            return (
                <Card className="artist" key={artist._id}>
                    <CardImg style={{width: '30%',  height: '140px'}} src={'http://localhost:8050/uploads/' + artist.image} alt="image" />
                    <CardBody style={{display: 'inline-block'}}>
                        <Link to={'/albums/' + artist._id}>{artist.title}</Link>
                    </CardBody>
                </Card>
            )
        });
        return (
            <div className="artists-block">
                <h2 className="artists-title">Artists</h2>
                {artists}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.musics.artists
});

const mapDispatchToProps = dispatch => ({
    fetchArtists: () => dispatch(fetchSuccess())
});
export default connect(mapStateToProps, mapDispatchToProps)(Artists);