import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchSuccess} from "../../store/actions/TracksAction";
import './Tracks.css'

class Track extends Component {

    componentDidMount() {
        this.props.fetchTracksSuccess(this.props.match.params.id);
    }

    render() {
        let albumName = '';
        let artistName = '';
        console.log(this.props.tracks);
        const tracks = this.props.tracks.map(track => {
            albumName = track.albums.title;
            artistName = track.artist;
            return (
                <div
                    key={track._id}
                    className="track"
                >
                    <div className="track-inner">
                        <span className="track-number">{track.trackNumber}.</span>
                        <span className="track-name">{track.title}</span>
                        <span className="track-duration">{track.duration}</span>
                        <i className="far fa-play-circle play-icon" />
                    </div>
                </div>
            )
        });
        return (
            <div className="tracks">
                <div className="tracks-inner">
                    <h2 className="artist-name">{artistName}</h2>
                    <h4 className="tracks-title">Tracks</h4>
                </div>
                <h4 className="album-name">{albumName}</h4>
                {tracks}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    tracks: state.musics.tracks
});

const mapDispatchToProps = dispatch => ({
    fetchTracksSuccess: (id) => dispatch(fetchSuccess(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Track);