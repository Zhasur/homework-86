import React, {Component} from 'react';
import {fetchSuccess} from "../../store/actions/AlbumActions";
import {connect} from "react-redux";
import {Card, CardBody, CardImg, CardTitle} from "reactstrap";

import './Album.css'

class Album extends Component {
    componentDidMount() {
        this.props.fetchAlbumSuccess(this.props.match.params.id);
    }


    render() {
        let artistName = '';
        const albums = this.props.albums.map(album => {
            artistName = album.artist.title;
           return (
               <Card onClick={() => this.props.history.push('/tracks/' + album._id)} className="album" key={album._id}>
                   <CardImg
                       style={{width: '30%',  height: '140px'}}
                       src={'http://localhost:8050/uploads/' + album.image}
                       alt="image"
                   />
                   <CardBody className="album-inner">
                       <CardTitle>{album.title}</CardTitle>
                       <CardTitle>Realised in: {album.releaseDate}</CardTitle>
                   </CardBody>
               </Card>
           )
        });
        return (
            <div className="albums">
                <div className="albums-header">
                    <h4 className="albums-title">Albums</h4>
                    <h2 className="artist-name">{artistName}</h2>
                </div>
                <div className="card-block">
                    {albums}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.musics.albums
});

const mapDispatchToProps = dispatch => ({
    fetchAlbumSuccess: (id) => dispatch(fetchSuccess(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Album);