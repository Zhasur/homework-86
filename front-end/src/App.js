import React, {Component, Fragment} from 'react';
import './App.css';
import {Route, Switch} from "react-router";
import Artists from "./containers/Artists/Artists";
import Album from "./containers/Album/Album";
import Track from "./containers/Tracks/Track";

class App extends Component {
  render() {
    return (
      <Fragment>
          <Switch>
              <Route path="/" exact component={Artists} />
              <Route path="/albums/:id" exact component={Album} />
              <Route path="/tracks/:id" exact component={Track} />
          </Switch>
      </Fragment>
    );
  }
}

export default App;
